##项目介绍
##WebApi的学习之通用接口的实现

##功能描述
##无需更改uri地址，即在同一地址下，通过改变方法即可修改数据库中的数据


##使用说明
##通过http://localhost:62776/ebu/api/v2/sjz/data?mappingId=SsArea.s_ss_area访问全部数据；
##通过相同地址，在body里将需要加入的数据以{"key1":value1,"key2":value2...}这样的格式写入
##然后通过POST/PUT/DELETE方式可对数据库进行数据的添加/修改/删除