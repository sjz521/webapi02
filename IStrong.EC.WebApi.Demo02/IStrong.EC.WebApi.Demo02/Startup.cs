﻿/************************************************************************************************************
* file    : Startup.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/4 9:49:33
************************************************************************************************************/
using IStrong.EC.DAO;
using IStrong.EC.DAO.Abstractions;
using IStrong.EC.DAO.Abstractions.Interfaces;
using IStrong.EC.DAO.Mapping.Xml;
using IStrong.EC.Web.Extensions;
using IStrong.EC.Web.Filters;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.FileProviders;
using System.IO;
using IStrong.EC.Abstractions.Extensions;
using System;
using AspectCore.Extensions.DependencyInjection;

namespace IStrong.EC.WebApi.Demo02
{
    /// <summary>
    /// 启动类
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// 构造函数
        /// </summary>
        /// <param name="configuration">依赖注入配置</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// 配置信息
        /// </summary>
        public IConfiguration Configuration { get; }


        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">服务容器</param>
        public IServiceProvider ConfigureServices(IServiceCollection services)
        {
            services.Configure<CookiePolicyOptions>(options =>
            {
                // This lambda determines whether user consent for non-essential cookies is needed for a given request.
                options.CheckConsentNeeded = context => true;
                options.MinimumSameSitePolicy = SameSiteMode.None;
            });
            services.AddGlobalOptions(this.Configuration);
            //添加EC框架提供的服务，如果需要往mvcoption或mvcbuilder里添加过滤器等请调用回调函数
            services.AddECServices();
            //注册mapping
            services.AddXmlMappingProvider();
            services.AddDatabaseFeature();
            //添加Session支持
            services.AddSession();
            //启用内存缓存
            services.AddMemoryCache();
            //启用跨域
            services.AddCors();
            services.AddHttpContextAccessor();
            services.AddHttpClient();

            //有用到批量插入和插入返回的需要加上以下服务
            //sqlserver数据库，安装IStrong.EC.DAO.SqlServer包
            //services.AddSqlServerBatcherProvider();
            //services.AddSqlServerReturning();
            //oracle数据库，安装IStrong.EC.DAO.Oracle包
            //services.AddOracleBatcherProvider();
            //services.AddOracleReturning();
            //mysql数据库，安装IStrong.EC.DAO.MySql包
            //services.AddMySqlBatcherProvider();
            //services.AddMySqlReturning();

            #region 本地化和全球化（如果需要请取消注释）

            //services.AddPortableObjectLocalization(opts => opts.ResourcesPath = "Resources");
            //services.AddMvc().AddViewLocalization(LanguageViewLocationExpanderFormat.Suffix);
            //services.Configure<RequestLocalizationOptions>(options =>
            //{
            //    var supportedCultures = new List<CultureInfo>
            //{
            //    new CultureInfo("en-US"),
            //    new CultureInfo("en"),
            //    new CultureInfo("fr-FR"),
            //    new CultureInfo("fr"),
            //    new CultureInfo("zh-cn")
            //};

            //    options.DefaultRequestCulture = new RequestCulture("zh-cn");
            //    options.SupportedCultures = supportedCultures;
            //    options.SupportedUICultures = supportedCultures;
            //});

            #endregion 本地化和全球化（如果需要请取消注释）

            return services.BuildAspectInjectorProvider();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app"></param>
        /// <param name="env"></param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            //添加EC框架提供的中间件
            app.UseECMiddlewares();
            //启用session
            app.UseSession();
            //通过请求支持本地化
            app.UseRequestLocalization();
            app.UseCookiePolicy();
            //集成swagger
            app.UseSwagger();
            //允许跨域
            app.UseCors(policy =>
            {
                policy.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
            });
            app.UseMvc(routes =>
            {
                routes.MapRoute(
                    name: "default",
                    template: "{controller=Home}/{action=Index}/{id?}");
            });
        }
    }
}
