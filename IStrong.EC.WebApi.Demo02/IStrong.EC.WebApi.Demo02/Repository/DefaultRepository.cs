﻿/************************************************************************************************************
* file    : DefaultRepository.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/4 9:49:33
************************************************************************************************************/
using IStrong.EC.Abstractions.Interfaces;
using IStrong.EC.DAO.Abstractions.Interfaces;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;

namespace IStrong.EC.WebApi.Demo02.Repository
{
    /// <summary>
    /// 数据访问类
    /// </summary>
    public class DefaultRepository : IService
    {
        /// <summary>
        /// 数据库操作组件
        /// </summary>
        private readonly IDbContext _db;

        /// <summary>
        /// 配置信息
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// 日志组件
        /// </summary>
        private readonly ILogger<DefaultRepository> _logger;

        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="db"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        public DefaultRepository(IDbContext db, ILogger<DefaultRepository> logger, IConfiguration configuration)
        {
            _db = db;
            _logger = logger;
            _configuration = configuration;
        }
    }
}
