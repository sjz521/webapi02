﻿/************************************************************************************************************
* file    : Default.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/4 9:49:33
************************************************************************************************************/
using IStrong.EC.Abstractions.Interfaces;
using System;
using System.Collections.Generic;
using System.Text;

namespace IStrong.EC.WebApi.Demo02.Entity
{
    /// <summary>
    /// 实体类模板
    /// </summary>
    public class Default : IEntity
    {
    }
}
