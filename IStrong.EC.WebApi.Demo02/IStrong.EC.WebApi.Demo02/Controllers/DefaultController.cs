﻿/************************************************************************************************************
* file    : DefaultController.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/4 9:49:33
************************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IStrong.EC.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace IStrong.EC.WebApi.Demo02.Controllers
{
    /// <summary>
    /// Api控制器模板
    /// </summary>
    [Route("api/[controller]")]
    [ApiController]
    public class DefaultController : BaseController
    {
        /// <summary>
        /// 日志
        /// </summary>
        private readonly ILogger<DefaultController> _logger;

        /// <summary>
        /// 配置
        /// </summary>
        private readonly IConfiguration _config;

        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        public DefaultController(ILogger<DefaultController> logger, IConfiguration config)
        {
            _logger = logger;
            _config = config;
        }

        /// <summary>
        /// GET api/default
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        /// <summary>
        /// GET api/default/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        /// <summary>
        /// POST api/default
        /// </summary>
        /// <param name="value"></param>
        [HttpPost]
        public void Post([FromBody] string value)
        {
        }

        /// <summary>
        /// PUT api/default/5
        /// </summary>
        /// <param name="id"></param>
        /// <param name="value"></param>
        [HttpPut("{id}")]
        public void Put(int id, [FromBody] string value)
        {
        }

        /// <summary>
        /// DELETE api/default/5
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete("{id}")]
        public void Delete(int id)
        {
        }
    }
}
