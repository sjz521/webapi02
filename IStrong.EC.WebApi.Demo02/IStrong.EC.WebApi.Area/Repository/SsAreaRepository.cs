﻿/************************************************************************************************************
* file    : DefaultRepository.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/4 9:50:40
************************************************************************************************************/
using System;
using System.Collections.Generic;
using IStrong.EC.Abstractions.Interfaces;
using IStrong.EC.DAO.Abstractions.Interfaces;
using IStrong.EC.WebApi.Area.Entity;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;

namespace IStrong.EC.WebApi.Area.Repository
{
    /// <summary>
    /// 数据访问类
    /// </summary>
    public class SsAreaRepository : IService
    {
        /// <summary>
        /// 数据库操作组件
        /// </summary>
        private readonly IDbContext _db;

        /// <summary>
        /// 配置信息
        /// </summary>
        private readonly IConfiguration _configuration;

        /// <summary>
        /// 日志组件
        /// </summary>
        private readonly ILogger<SsAreaRepository> _logger;

        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="db"></param>
        /// <param name="configuration"></param>
        /// <param name="logger"></param>
        public SsAreaRepository(IDbContext db, ILogger<SsAreaRepository> logger, IConfiguration configuration)
        {
            _db = db;
            _logger = logger;
            _configuration = configuration;
        }

        /// <summary>
        /// 获取信息
        /// </summary>
        /// <param name="orderId"></param>
        /// <returns></returns>
        public IEnumerable<SsArea> GetAreaInfo(string orderId = null)
        {
            return _db.Query<SsArea>("SsArea.s_ss_area", new { orderId });
        }
        /// <summary>
        /// 测试插入数据
        /// </summary>
        public SsArea InsertAreaInfo(SsArea ssArea)
        {
            string connName = "Conn";
            string sql = "INSERT INTO SS_AREA(ORDER_ID,AREA,POPUNUM,LOCATION,FLOODTASK,ADDTIME,UPDTIME,ADDUSERID,UPDUSERID,REMARK,SHORTNAME,ADCD,ADNM,LGTD,LTTD,MINX,MINY,MAXX,MAXY,MASTER,MOBILE,FAX,TYPHOONLAND,REALADCD) VALUES(@OrderId,@Area,@Popunum,@Location,@Floodtask,@Addtime,@Updtime,@Adduserid,@Upduserid,@Remark,@Shortname,@Adcd,@Adnm,@Lgtd,@Lttd,@Minx,@Miny,@Maxx,@Maxy,@Master,@Mobile,@Fax,@Typhoonland,@Realadcd)";
            _db.Execute(connName,sql, ssArea);//返回影响行数
            return ssArea;
        }
        /// <summary>
        /// 测试更新
        /// </summary>
        /// <returns></returns>
        public SsArea UpdateAreaInfo(SsArea ssArea)
        {
            string connName = "Conn";
            string sql = "UPDATE SS_AREA SET ORDER_ID=@OrderId,AREA=@Area,POPUNUM=@Popunum,LOCATION=@Location,FLOODTASK=@Floodtask,ADDTIME=@Addtime,UPDTIME=@Updtime,ADDUSERID=@Adduserid,UPDUSERID=@Upduserid,REMARK=@Remark,SHORTNAME=@Shortname,ADCD=@Adcd,ADNM=@Adnm,LGTD=@Lgtd,LTTD=@Lttd,MINX=@Minx,MINY=@Miny,MAXX=@Maxx,MAXY=@Maxy,MASTER=@Master,MOBILE=@Mobile,FAX=@Fax,TYPHOONLAND=@Typhoonland,REALADCD=@Realadcd  WHERE ADCD=@Adcd";
            _db.Execute(connName,sql, ssArea);
            return ssArea;
        }
        /// <summary>
        /// 测试删除数据，并将要删除的数据返回
        /// </summary>
        /// <param name="ssArea"></param>
        /// <returns></returns>
        public IEnumerable<SsArea> DeleteAreaInfo(string adcd)
        {
            string connName = "Conn";
            var result = SearchAreaInfo(adcd);
            string sql = "DELETE FROM SS_AREA WHERE ADCD=@Adcd";
            _db.Execute(connName,sql, new { ADCD = adcd});
            return result;
        }
        /// <summary>
        /// 根据adcd查询信息
        /// </summary>
        /// <param name="adcd"></param>
        /// <returns></returns>
        public IEnumerable<SsArea> SearchAreaInfo(string adcd)
        {
            return _db.Query<SsArea>("SsArea.s_ss_area", new { ADCD = adcd });
        }
    }
}
