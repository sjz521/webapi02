﻿/************************************************************************************************************
* file    : DefaultController.cs
* author  : LTBLV12374IVQQ5
* function: 
* history : created by LTBLV12374IVQQ5 2019/7/4 9:50:40
************************************************************************************************************/
using IStrong.EC.WebApi.Area.Repository;
using IStrong.EC.Web.Controllers;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Text;
using IStrong.EC.WebApi.Area.Entity;

namespace IStrong.EC.WebApi.Area.Controller
{
    /// <summary>
    /// Api控制器模板
    /// </summary>
    [Route("ebu/api/v2/sjz/data")]
    [ApiController]
    public class SsAreaController : BaseController
    {
        /// <summary>
        /// 日志
        /// </summary>
        private readonly ILogger<SsAreaController> _logger;

        /// <summary>
        /// 配置
        /// </summary>
        private readonly IConfiguration _config;

        private readonly SsAreaRepository _repository;

        /// <summary>
        /// 注入服务
        /// </summary>
        /// <param name="logger"></param>
        /// <param name="config"></param>
        /// <param name="repository"></param>
        public SsAreaController(ILogger<SsAreaController> logger, IConfiguration config, SsAreaRepository repository)
        {
            _logger = logger;
            _config = config;
            _repository = repository;
        }

        /// <summary>
        /// GET 
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        [Obsolete]
        public ActionResult<IEnumerable<string>> Get(string mappingId)
        {
            return Json(_repository.GetAreaInfo(null));
        }

        /// <summary>
        /// GET
        /// </summary>
        /// <returns></returns>
        /*[HttpGet]
        [Obsolete]
        public ActionResult<IEnumerable<string>> Get(string mappingId,string sort)
        {
            var list = _repository.SortAreaInfo(sort);
            return Json(list);
        }*/

        /// <summary>
        /// GET api/values/5
        /// </summary>
        /// <param name="id"></param>
        /// <returns></returns>
        /*[HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }*/

        /// <summary>
        /// POST api/values
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="ssArea"></param>
        [HttpPost]
        [Obsolete]
        public ActionResult<string> Post(string mappingId, [FromBody] SsArea ssArea)
        {
            if (ssArea == null)
            {
                throw new ArgumentException("没有数据！！");
            }
            return Json(_repository.InsertAreaInfo(ssArea));
        }

        /// <summary>
        /// PUT api/values/5
        /// </summary>
        /// <param name="mappingId"></param>
        /// <param name="ssArea"></param>
        [HttpPut]
        [Obsolete]
        public ActionResult<string> Put(string mappingId, [FromBody] SsArea ssArea)
        {
            if (ssArea == null)
            {
                throw new ArgumentException("没有数据！！");
            }
            return Json(_repository.UpdateAreaInfo(ssArea));
        }

        /// <summary>
        /// DELETE api/values/5
        /// </summary>
        /// <param name="id"></param>
        [HttpDelete]
        [Obsolete]
        public ActionResult<string> Delete([FromBody]SsArea ssArea)
        {
            if (ssArea.Adcd == null || ssArea.Adcd == "")
            {
                throw new ArgumentException("请给adcd赋值！！");
            }
            return Json(_repository.DeleteAreaInfo(ssArea.Adcd));

        }
    }
}
